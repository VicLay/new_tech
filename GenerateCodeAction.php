<?php

namespace App\Users\Http\Actions;

use App\Users\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\RateLimiter;

class GenerateCodeAction
{
    const CONFIRM_CODE_CACHE_KEY = 'user_id:%d:confirm_code:%s';
    const ALL_DIGITS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const PATTERNS_PROBABILITY = [
        '2_identical_digits' => 30,                       // (2 одинаковые цифры)
        '2_identical_digits_in_a_row' => 25,              // (2 одинаковые цифры)
        '2_character_paired_matches' => 15,               // (2-ух символьные парные совпадения)
        '2_pairs_of_2_identical_digits_in_a_row' => 10,   // (2 пары 2ух одинаковых цифр подряд)
        'ladder' => 7,                                    // (лесенка)
        'reverse_ladder' => 7,                            // (обратная лесенка)
        '3_identical_digits_in_a_row' => 5,               // (3 одинаковые цифры подряд)
        'single_digit_code' => 1,                         // (код составлен из одной цифры)
    ];

    public function __invoke($userId): JsonResponse
    {
//        if (!RateLimiter::attempt('confirm-code-attempt:' . $userId, 3, fn() => '')) {
//            return response()->json(['error' => 'Too Many Requests'], 429);
//        }

        /** @var User $user */
        if (!$user = User::find($userId)) {
            return response()->json('The user was not found', 404);
        }

        return response()->json(['confirm_code' => $this->generateRandomUniqueUserCode($user->getKey())]);
    }

    protected function generateRandomUniqueUserCode(int $userId): string
    {
        do {
            $rand = mt_rand(1, 100);
            $probability = 0;

            switch (true) {
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['2_identical_digits']):
                    $digit1 = $digit2 = mt_rand(0, 9);
                    $digit3 = $this->getRandDigitWithExclude($digit1);
                    $digit4 = $this->getRandDigitWithExclude($digit1, $digit3);

                    $codeArray = [$digit1, $digit2, $digit3, $digit4];
                    shuffle($codeArray);
                    $code = implode('', $codeArray);
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['2_identical_digits_in_a_row']):
                    $digit1 = mt_rand(0, 9);
                    $doubleDigit = $digit1 . $digit1;
                    $digit3 = $this->getRandDigitWithExclude($digit1);
                    $digit4 = $this->getRandDigitWithExclude($digit1, $digit3);
                    $codeArray = [$doubleDigit, $digit3, $digit4];

                    shuffle($codeArray);
                    $code = implode('', $codeArray);
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['2_character_paired_matches']):
                    $digit1 = mt_rand(0, 9);
                    $digit2 = $this->getRandDigitWithExclude($digit1);

                    $code = $digit1 . $digit2 . $digit1 . $digit2;
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['2_pairs_of_2_identical_digits_in_a_row']):
                    $digit1 = mt_rand(0, 9);
                    $digit2 = $this->getRandDigitWithExclude($digit1);

                    $code = $digit1 . $digit1 . $digit2 . $digit2;
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['ladder']):
                    $digit = mt_rand(0, 6);

                    $code = $digit . ($digit + 1) . ($digit + 2) . ($digit + 3);
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['reverse_ladder']):
                    $digit = mt_rand(3, 9);

                    $code = $digit . ($digit - 1) . ($digit - 2) . ($digit - 3);
                    break;
                case ($rand <= $probability += self::PATTERNS_PROBABILITY['3_identical_digits_in_a_row']):
                    $digit = mt_rand(0, 9);

                    $code = $digit . $digit . $digit . $this->getRandDigitWithExclude($digit);
                    break;
                default:
                    $digit = mt_rand(0, 9);
                    $code = $digit . $digit . $digit . $digit;
            }
        } while (Cache::has(sprintf(self::CONFIRM_CODE_CACHE_KEY, $userId, $code)));

        Cache::set(sprintf(self::CONFIRM_CODE_CACHE_KEY, $userId, $code), true, 300);

        return $code;
    }

    protected function getRandDigitWithExclude(...$excludeArray): string
    {
        $availableDigits = array_diff(self::ALL_DIGITS, $excludeArray);

        return $availableDigits[array_rand($availableDigits)];
    }
}

